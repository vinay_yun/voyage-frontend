import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import Home from './containers/homeComponent';
import { Route, Link, Redirect, withRouter } from 'react-router-dom';
import {
  Menu,
  Icon
} from 'semantic-ui-react';
import './styles/tab.css';
import './styles/NavMenu.css';
// import ShipVoyages from './containers/voyagesComponent';
import FleetAnalytics from './containers/fleetAnalyticsComponent';
import ServiceDetails from './containers/serviceDetailsComponent';
import LiveHealth from './containers/liveHealthComponent';
import Settings from './containers/settingsComponent';
import Login from './containers/loginComponent';

class TransHubMenu extends Component {
  constructor(props) {
    super(props);
    this.handleLogOut = this.handleLogOut.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  handleItemClick(e, { name }) {
    this.props.dispatch({ type: 'TAB_CHANGED', payload: name });
  }

  handleLogOut(e) {
    //console.log("logged out", e)
    this.props.dispatch({ type: 'USER_LOGGED_OUT', payload: null });
    this.props.dispatch({ type: 'TAB_RESET', payload: null });
  }
  render() {
    return (
      <React.Fragment>
        <Menu className='fleetmenu' secondary>
          <Menu.Item
            name='fleet map'
            active={this.props.tabData.tab === 'fleet map'}
            onClick={this.handleItemClick}
            as={Link}
            to='/'
          />
          <Menu.Item
            name='live tracking'
            active={this.props.tabData.tab === 'live tracking'}
            onClick={this.handleItemClick}
            as={Link}
            to='/livetracking'
          />

          <Menu.Item
            name='services'
            active={this.props.tabData.tab === 'services'}
            onClick={this.handleItemClick}
            as={Link}
            to='/services'
          />
          <Menu.Item
            name='analytics'
            active={this.props.tabData.tab === 'analytics'}
            onClick={this.handleItemClick}
            as={Link}
            to='/analytics'
          />
          <Menu.Item
            name='settings'
            active={this.props.tabData.tab === 'settings'}
            onClick={this.handleItemClick}
            as={Link}
            to='/settings'
          />
          <Menu.Menu position='right'>
            <Menu.Item>Hi, {this.props.authData.userName}</Menu.Item>
            <Menu.Item
              name='inbox'
              active={this.props.tabData.tab === 'inbox'}
              onClick={this.handleItemClick}
              as={Link}
              to='/inbox'
            />
            <Menu.Item
              name='help'
              active={this.props.tabData.tab === 'help'}
              onClick={this.handleItemClick}
              as={Link}
              to='/help'
            />

            <Menu.Item
              name='log out'
              active={this.props.tabData.tab === 'log out'}
              onClick={this.handleLogOut}
              as={Link}
              to='/logout'
            >
              <span style={{ fontWeight: 'bold', paddingRight: '10px' }}>
                Log Out
              </span>
              <Icon name='sign out' />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </React.Fragment>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className='App'>
        {this.props.authData.loginSuccess ? <TransHubMenuConnected /> : null}
        <main>
          <Route exact path='/' render={() =>
            this.props.authData.loginSuccess ? (
              <Redirect to='/fleetmap' />
            ) : (
                <Redirect to="/login" />
              )} />
          
          <Route exact path="/login" render={() => (
            this.props.authData.loginSuccess ? (
              <Redirect to="/fleetmap" />
            ) : (
                <Login />
              )
          )} />

          <Route exact path='/fleetmap' render={() => <Home />} />

          {/* <Route exact path="/voyage" render={() => (
              <ShipVoyages />
          )} /> */}

          <Route exact path='/livetracking' render={() => <LiveHealth />} />

          <Route exact path='/analytics' render={() => <FleetAnalytics />} />

          <Route exact path='/services' render={() => <ServiceDetails />} />

          <Route exact path='/settings' render={() => <Settings />} />

          <Route exact path="/logout" render={() => (
            <Redirect to="/login" />
          )} />
        </main>

      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  // console.log(globalData)
  return {
    tabData: globalData.tabData,
    authData: globalData.authData
  };
}

const myConnector = connect(mapStatesToProps);
const TransHubMenuConnected = myConnector(TransHubMenu);
export default withRouter(myConnector(App));
