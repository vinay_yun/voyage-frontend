/* global google */
import React, { Component } from 'react';
import {
  withGoogleMap,
  GoogleMap,
  Marker,
  TrafficLayer,
  Polyline,
  InfoWindow
} from 'react-google-maps';
import { connect } from 'react-redux';
import * as activeCar from '../images/mapComponent/ship.png';
import * as inactiveCar from '../images/mapComponent/ship.png';
// import { fetchAddressByGeocoding, calculateEtaDistance } from '../actions/mapActions';
// import {updatePassword} from '../actions/loginActions'
import { Input, Form, Button, Modal } from 'semantic-ui-react';
// import ReactInterval from 'react-interval';
const {
  StandaloneSearchBox
} = require('react-google-maps/lib/components/places/StandaloneSearchBox');

class MapView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null,
      searchBox: null,
      count: 0,
      animatePoly: {},
      boolcounter: false
    };
  }

  showNormalPolyline() {
    let tempPoly = [];
    tempPoly = this.props.voyageLocations;
    if (tempPoly.length > 0) {
      return (
        <div>
          <Polyline
            key={42}
            options={{
              //
              strokeOpacity: 1.0,
              strokeWeight: 3,
              visible: true
            }}
            path={tempPoly}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  mapLoaded(map) {
    if (this.state.map != null) {
      return;
    }
    this.setState({
      map: map
    });
  }

  markerClicked(deviceID) {
    this.props.dispatch({ type: 'MARKER_CLICKED', payload: deviceID });
  }

  showInfoWindow() {
    // console.log("hello", this.props.deviceLocations);
    return this.props.deviceLocations.map((location, index) => {
      if (location.showInfobox) {
        // this.props.dispatch(fetchAddressByGeocoding(location.deviceID, location.lat, location.lng));
        var inlocation = location.lat + ',' + location.lng;
        // this.props.dispatch(calculateEtaDistance(this.props.authData.token, this.props.outLocation, inlocation));
        var str = location.time;
        var dateTime = str.split(' ');
        return (
          <InfoWindow
            key={location.id}
            onCloseClick={this.infoWindowClosed.bind(this, location.deviceID)}
            position={{ lat: location.lat, lng: location.lng }}
          >
            <div className='mapInfoBox'>
              <h4>
                Vehicle Registration No :{' '}
                <span className='mapInfoSpan'>{location.carRegiNum} </span>{' '}
              </h4>
              <h5>
                Make : <span className='mapInfoSpan'>{location.carMake} </span>
              </h5>
              <h5>
                Model :{' '}
                <span className='mapInfoSpan'>{location.carModel} </span>
              </h5>
              <h5>
                Driver Name :{' '}
                <span className='mapInfoSpan'>{location.dName} </span>
              </h5>
              <h5>
                Device ID :{' '}
                <span className='mapInfoSpan'>{location.deviceID} </span>
              </h5>
              <h5>
                Vehicle Speed :{' '}
                <span className='mapInfoSpan'>{location.speed} Km/Hr </span>
              </h5>
              <h5>
                Date : <span className='mapInfoSpan'>{dateTime[0]} </span> Time
                : <span className='mapInfoSpan'>{dateTime[1]} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.infoContent} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.outLocation} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.distance} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.eta} </span>
              </h5>
            </div>
          </InfoWindow>
        );
      } else {
        return null;
      }
    });
  }

  infoWindowClosed(deviceId) {
    this.props.dispatch({ type: 'INFO_WINDOW_CLOSED', payload: deviceId });
  }

  showMarkers() {
    console.log('show marker', this.props.deviceLocations);
    return this.props.deviceLocations.map((location, index) => {
      if (location.showMarker) {
        let icon_url = activeCar;
        let deviceID = 'DeviceID - ' + location.deviceID;
        const diff = new Date().getTime() - new Date(location.time).getTime();
        if (diff / 1000 / 60 > 1) {
          icon_url = inactiveCar;
        }
        return (
          <Marker
            position={{ lat: location.lat, lng: location.lng }}
            animation={google.maps.Animation.DROP}
            title={deviceID}
            icon={{
              url: icon_url
            }}
            origin={{ lat: 7, lng: 50 }}
            key={index * 10}
            onClick={this.markerClicked.bind(this, location.deviceID)}
          />
        );
      } else {
        return null;
      }
    });
  }

  polylineclick() {
    console.log('polyline click', this.state);
    return (
      <InfoWindow position={{ lat: 18.6487454, lng: 73.9897163 }}>
        <div>
          <h4>{this.props.infoContent}</h4>
          <h4>{this.props.outLocation}</h4>
          <h4>{this.props.distance}</h4>
          <h4>{this.props.eta}</h4>
        </div>
      </InfoWindow>
    );
  }

  showPolylines() {
    // return this.props.deviceLocations.map((location, index) => {
    // 	let arr = this.props.polylineDict[location.deviceID]
    // if(arr.length >1){
    // 	return <Polyline
    // 		key={index}
    // 		path={arr}>
    // 	</Polyline>
    // }
    // else {
    // 	return null
    // }
    // })
    //{(arr.length >= this.state.count) ? arr[this.state.count] : arr[arr.length - 1]}
    let arr = this.props.polyline;
    //let arr = this.state.animatePoly
    if (arr.length) {
      console.log('polyline route', arr);
      var icon = activeCar;
      return (
        <div>
          <Marker
            position={arr[arr.length - 1]}
            animation={google.maps.Animation.DROP}
            title={'Destination'}
            icon={{
              url: icon
            }}
          />
          <Polyline
            options={{
              strokeOpacity: 1.0,
              strokeWeight: 3,
              visible: true
            }}
            path={arr}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  showStartEndMarkers() {
    if ('lat' in this.props.startLoc) {
      return (
        <div>
          <Marker
            position={{
              lat: this.props.startLoc.lat,
              lng: this.props.startLoc.lng
            }}
            animation={google.maps.Animation.DROP}
            title={'Start Point'}
            key={1}
            icon={{
              url: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
              strokeColor: 'red',
              scale: 3
            }}
            //onClick={this.markerClicked.bind(this, location.deviceID)}
          />
          <Marker
            position={{
              lat: this.props.endLoc.lat,
              lng: this.props.endLoc.lng
            }}
            animation={google.maps.Animation.DROP}
            title={'End Point'}
            key={2}
            icon={{
              url: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
              strokeColor: 'blue',
              scale: 3
            }}
            //onClick={this.markerClicked.bind(this, location.deviceID)}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <div id='Map'>
        <GoogleMap
          ref={this.mapLoaded.bind(this)}
          defaultZoom={4}
          defaultCenter={{ lat: 0.654673, lng: 105.407098 }}
          center={this.props.mainMapCenter}
        >
          {this.showNormalPolyline()}
          {this.showStartEndMarkers()}
          {/* <TrafficLayer autoUpdate /> */}
          {/* {this.showMarkers()} */}
          {/* {this.showInfoWindow()} */}
          {/* {this.showPolylines()} */}
        </GoogleMap>
        {/* <StandaloneSearchBox 
					ref={this.onSearchBoxMounted.bind(this)}
					controlPosition={google.maps.ControlPosition.BOTTOM_LEFT}
					onPlacesChanged={this.onPlacesChanged.bind(this)}
				>
					<input
						type="text"
						placeholder="Enter destination"
						style={{
							//boxSizing: `border-box`,
							marginLeft: `20%`,
							border: `1px solid transparent`,
							width: `360px`,
							height: `32px`,
							marginTop: `10px`,
							padding: `0 12px`,
							borderRadius: `3px`,
							boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
							//fontSize: `14px`,
							//outline: `none`,
							//textOverflow: `ellipses`,
						}}
					/>
				</StandaloneSearchBox>
				<Button align="right" onClick={this.handleCalculateEta}>
					Calculate ETA
        			</Button>
				<Button align="right">
					Get route(no use)
        			</Button> */}
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    // authData: globalData.authData,
    // tabData: globalData.tabData,
    deviceLocations: globalData.mapSearchData.deviceLocations,
    polylineDict: globalData.mapSearchData.polylineDict,
    infoContent: globalData.mapSearchData.infoContent,
    outLocation: globalData.mapSearchData.outLocation,
    distance: globalData.mapSearchData.distance,
    eta: globalData.mapSearchData.eta,
    polyline: globalData.mapSearchData.polyline,
    animatePolyline: globalData.mapSearchData.animatePolyline,
    mainMapCenter: globalData.mapSearchData.mainMapCenter,
    voyageLocations: globalData.voyagesData.voyageLocations,
    mainMapCenter: globalData.mapSearchData.mainMapCenter,
    startLoc: globalData.voyagesData.startLoc,
    endLoc: globalData.voyagesData.endLoc
    // firstLogin: globalData.authData.firstLogin,
    // openPasswordChangeModal: globalData.authData.openPasswordChangeModal,
    // newPasswordText: globalData.authData.newPasswordText,
    // oldPasswordText: globalData.authData.oldPasswordText,
    // newPasswordUpdateMessage: globalData.authData.newPasswordUpdateMessage,
    // newPasswordUpdated: globalData.authData.newPasswordUpdated,
    // retypeNewPasswordText: globalData.authData.retypeNewPasswordText
  };
}

export default connect(mapStatesToProps)(withGoogleMap(MapView));
