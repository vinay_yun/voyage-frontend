import React, { Component } from 'react';
import { connect } from 'react-redux';
// import SocketConnection from '../containers/socketComponent';
import { Input, Table, Label } from 'semantic-ui-react';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleCheckBoxStatusChange = this.handleCheckBoxStatusChange.bind(
      this
    );
  }

  handleFilterTextChange(e) {
    this.props.dispatch({
      type: 'SEARCH_TEXT_CHANGED',
      payload: e.target.value
    });
  }

  handleCheckBoxStatusChange(e) {
    console.log('checkBox', e.target.checked);
    this.props.dispatch({
      type: 'CHECKBOX_STATE_CHANGED',
      payload: e.target.checked
    });
  }

  render() {
    const filterText = this.props.filterText;
    return (
      <form className='vehicleSearch'>
        <Input
          type='text'
          focus
          floated='left'
          placeholder='Search...'
          value={filterText}
          onChange={this.handleFilterTextChange}
        />
        <br />

        <Label basic style={{ marginTop: '5px' }}>
          Select All
        </Label>
        <input
          type='checkbox'
          style={{ marginRight: '10%', marginTop: '15px' }}
          checked={this.props.checkboxstate}
          onChange={this.handleCheckBoxStatusChange}
        />
      </form>
    );
  }
}

class SearchList extends Component {
  listCheckBoxStatusChange(id) {
    console.log('clicked.', id);
    this.props.dispatch({ type: 'LIST_CHECKBOX_STATUS_CHANGED', payload: id });
  }

  createSearchListItems() {
    if (this.props.seachlistitems.length !== 0) {
      return this.props.seachlistitems.map((searchlistitem, index) => {
        if (
          searchlistitem.carRegiNum.indexOf(
            this.props.searchtext.toUpperCase()
          ) !== -1 ||
          searchlistitem.dDeviceID.toString().indexOf(this.props.searchtext) !==
            -1
        ) {
          return (
            <Table.Row verticalAlign='top' key={index}>
              <Table.Cell>
                {searchlistitem.carRegiNum}
                <br />
                {searchlistitem.dName}
              </Table.Cell>
              <Table.Cell>{searchlistitem.dDeviceID}</Table.Cell>
              <Table.Cell>
                <input
                  type='checkbox'
                  floated='right'
                  checked={searchlistitem.deviceChecked}
                  onChange={this.listCheckBoxStatusChange.bind(
                    this,
                    searchlistitem.dDeviceID
                  )}
                />
              </Table.Cell>
            </Table.Row>
          );
        } else {
          return null;
        }
      });
    }
  }

  render() {
    return (
      <div className='mainSearchList'>
        <div>
          <SearchBox />
          <Table className='deviceSearchTable'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Vessel name</Table.HeaderCell>
                <Table.HeaderCell>Voyage Number</Table.HeaderCell>
                <Table.HeaderCell />
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.createSearchListItems()}</Table.Body>
          </Table>
        </div>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    seachlistitems: globalData.mapSearchData.searchlist,
    searchtext: globalData.mapSearchData.searchtext,
    checkboxstate: globalData.mapSearchData.checkboxstate,
    reloadSearchList: globalData.mapSearchData.reloadSearchList
  };
}

const myConnector = connect(mapStatesToProps);
const SearchBox = myConnector(SearchBar);
export default myConnector(SearchList);
