/* global google */
import React, { Component } from 'react';
import {
  withGoogleMap,
  GoogleMap,
  Marker,
  Polyline,
  InfoWindow
} from 'react-google-maps';
import { connect } from 'react-redux';
import * as activeCar from '../images/mapComponent/ship.png';
import * as inactiveCar from '../images/mapComponent/ship.png';
// import { Input, Form, Button, Modal } from 'semantic-ui-react';
// const { StandaloneSearchBox } = require("react-google-maps/lib/components/places/StandaloneSearchBox");

class MapView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null,
      searchBox: null,
      count: 0,
      animatePoly: {},
      boolcounter: false
    };
    this.oldPasswordChange = this.oldPasswordChange.bind(this);
    this.newPasswordChange = this.newPasswordChange.bind(this);
    this.retypeNewPasswordChange = this.retypeNewPasswordChange.bind(this);
    this.submitNewPassword = this.submitNewPassword.bind(this);
    this.passwordChangeModalClose = this.passwordChangeModalClose.bind(this);
  }

  onSearchBoxMounted(ref) {
    // this.state.searchBox = ref;
    this.setState({
      searchBox: ref
    })
  }

  onPlacesChanged() {
    const places = this.state.searchBox.getPlaces();
    return places.map((place, index) => {
      // var position = place.geometry.location;
      // var location = { lat: position.lat(), lng: position.lng() };
      return this.props.dispatch({
        type: 'ON_PLACES_CHANGED',
        payload: places[0].formatted_address
      });
    });
  }

  mapLoaded(map) {
    if (this.state.map != null) {
      return;
    }
    this.setState({
      map: map
    });
  }

  markerClicked(deviceID) {
    this.props.dispatch({ type: 'MARKER_CLICKED', payload: deviceID });
  }

  showInfoWindow() {
    // console.log("hello", this.props.deviceLocations);
    return this.props.deviceLocations.map((location, index) => {
      if (location.showInfobox) {
        // this.props.dispatch(fetchAddressByGeocoding(location.deviceID, location.lat, location.lng));
        // var inlocation = location.lat + ',' + location.lng;
        // this.props.dispatch(calculateEtaDistance(this.props.authData.token, this.props.outLocation, inlocation));
        var str = location.time;
        var dateTime = str.split(' ');
        return (
          <InfoWindow
            key={location.id}
            onCloseClick={this.infoWindowClosed.bind(this, location.deviceID)}
            position={{ lat: location.lat, lng: location.lng }}
          >
            <div className='mapInfoBox'>
              <h4>
                Vehicle Registration No :{' '}
                <span className='mapInfoSpan'>{location.carRegiNum} </span>{' '}
              </h4>
              <h5>
                Make : <span className='mapInfoSpan'>{location.carMake} </span>
              </h5>
              <h5>
                Model :{' '}
                <span className='mapInfoSpan'>{location.carModel} </span>
              </h5>
              <h5>
                Driver Name :{' '}
                <span className='mapInfoSpan'>{location.dName} </span>
              </h5>
              <h5>
                Device ID :{' '}
                <span className='mapInfoSpan'>{location.deviceID} </span>
              </h5>
              <h5>
                Vehicle Speed :{' '}
                <span className='mapInfoSpan'>{location.speed} Km/Hr </span>
              </h5>
              <h5>
                Date : <span className='mapInfoSpan'>{dateTime[0]} </span> Time
                : <span className='mapInfoSpan'>{dateTime[1]} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.infoContent} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.outLocation} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.distance} </span>
              </h5>
              <h5>
                <span className='mapInfoSpan'>{this.props.eta} </span>
              </h5>
            </div>
          </InfoWindow>
        );
      } else {
        return null;
      }
    });
  }

  infoWindowClosed(deviceId) {
    this.props.dispatch({ type: 'INFO_WINDOW_CLOSED', payload: deviceId });
  }

  showMarkers() {
    console.log('show marker', this.props.deviceLocations);
    return this.props.deviceLocations.map((location, index) => {
      if (location.showMarker) {
        let icon_url = activeCar;
        let deviceID = 'DeviceID - ' + location.deviceID;
        const diff = new Date().getTime() - new Date(location.time).getTime();
        if (diff / 1000 / 60 > 1) {
          icon_url = inactiveCar;
        }
        return (
          <Marker
            position={{ lat: location.lat, lng: location.lng }}
            animation={google.maps.Animation.DROP}
            title={deviceID}
            icon={{
              url: icon_url
            }}
            origin={{ lat: 7, lng: 50 }}
            key={index * 10}
            onClick={this.markerClicked.bind(this, location.deviceID)}
          />
        );
      } else {
        return null;
      }
    });
  }

  polylineclick() {
    console.log('polyline click', this.state);
    return (
      <InfoWindow position={{ lat: 18.6487454, lng: 73.9897163 }}>
        <div>
          <h4>{this.props.infoContent}</h4>
          <h4>{this.props.outLocation}</h4>
          <h4>{this.props.distance}</h4>
          <h4>{this.props.eta}</h4>
        </div>
      </InfoWindow>
    );
  }

  showPolylines() {
    // return this.props.deviceLocations.map((location, index) => {
    // 	let arr = this.props.polylineDict[location.deviceID]
    // if(arr.length >1){
    // 	return <Polyline
    // 		key={index}
    // 		path={arr}>
    // 	</Polyline>
    // }
    // else {
    // 	return null
    // }
    // })
    //{(arr.length >= this.state.count) ? arr[this.state.count] : arr[arr.length - 1]}
    let arr = this.props.polyline;
    //let arr = this.state.animatePoly
    if (arr.length) {
      console.log('polyline route', arr);
      var icon = activeCar;
      return (
        <div>
          <Marker
            position={arr[arr.length - 1]}
            animation={google.maps.Animation.DROP}
            title={'Destination'}
            icon={{
              url: icon
            }}
          />
          <Polyline
            options={{
              strokeOpacity: 1.0,
              strokeWeight: 3,
              visible: true
            }}
            path={arr}
          />
        </div>
      );
    } else {
      return null;
    }
  }

  startCounter() {
    this.setState({ boolcounter: true });
  }

  animatePolyline() {
    this.setState({ count: this.state.count + 1 });
    let arr = this.props.polyline;
    if (arr.length) {
      if (arr.length > this.state.count) {
        let locArray = [];
        for (let i = 0; i <= this.state.count; i++) {
          locArray.push(arr[i]);
        }
        this.setState({ animatePoly: locArray });
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  handleCalculateEta() {
    console.log('calculating eta');
    if (this.props.outLocation !== '') {
      // this.props.dispatch(calculateEtaDistance(this.props.authData.token, this.props.outLocation, inlocation));
    }
  }
  newPasswordChange(e) {
    this.props.dispatch({
      type: 'NEW_PASSWORD_TEXT_CHANGE',
      payload: e.target.value
    });
  }
  retypeNewPasswordChange(e) {
    this.props.dispatch({
      type: 'RETYPE_NEW_PASSWORD_TEXT_CHANGE',
      payload: e.target.value
    });
  }
  oldPasswordChange(e) {
    this.props.dispatch({
      type: 'OLD_PASSWORD_TEXT_CHANGE',
      payload: e.target.value
    });
  }
  submitNewPassword() {
    if (this.props.retypeNewPasswordText !== this.props.newPasswordText) {
      alert('New password and Confirm new password are not same');
    }
  }
  passwordChangeModalClose() {
    this.props.dispatch({
      type: 'PASSWORD_CHANGE_MODAL_CLOSED',
      payload: false
    });
  }
  render() {
    return (
      <div id='Map'>
        <GoogleMap
          ref={this.mapLoaded.bind(this)}
          defaultZoom={6}
          defaultCenter={{ lat: 0.654673, lng: 105.407098 }}
          center={this.props.mainMapCenter}
        >
          {this.showMarkers()}
        </GoogleMap>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    // authData: globalData.authData,
    // tabData: globalData.tabData,
    deviceLocations: globalData.mapSearchData.deviceLocations,
    polylineDict: globalData.mapSearchData.polylineDict,
    infoContent: globalData.mapSearchData.infoContent,
    outLocation: globalData.mapSearchData.outLocation,
    distance: globalData.mapSearchData.distance,
    eta: globalData.mapSearchData.eta,
    polyline: globalData.mapSearchData.polyline,
    animatePolyline: globalData.mapSearchData.animatePolyline,
    mainMapCenter: globalData.mapSearchData.mainMapCenter
    // firstLogin: globalData.authData.firstLogin,
    // openPasswordChangeModal: globalData.authData.openPasswordChangeModal,
    // newPasswordText: globalData.authData.newPasswordText,
    // oldPasswordText: globalData.authData.oldPasswordText,
    // newPasswordUpdateMessage: globalData.authData.newPasswordUpdateMessage,
    // newPasswordUpdated: globalData.authData.newPasswordUpdated,
    // retypeNewPasswordText: globalData.authData.retypeNewPasswordText
  };
}

export default connect(mapStatesToProps)(withGoogleMap(MapView));
