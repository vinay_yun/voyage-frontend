import { applyMiddleware, createStore, compose } from 'redux';
// import { createLogger } from 'redux-logger';
//import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './reducers';
import {loadState,saveState} from "./localStorage";

const persistedState = loadState();

const middleware = applyMiddleware(promise(), /*routerMiddleware(history),*/ thunk);
// const middleware = applyMiddleware(promise(), thunk, createLogger())

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer,persistedState,composeEnhancers(middleware));

// use throttle from iodash if you fee that state is getting stored too frequently
store.subscribe(()=>{
    saveState({
        authData: store.getState().authData,
        tabData: store.getState().tabData

    });
});

export const history = createHistory();

export default store;