import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Divider, Button, Table, Segment, Grid, Icon } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

class LiveHealth extends Component {

  addCell(speedObj) {
    if (speedObj.status === 'normal') {
      return (
        <Table.Cell>
          <Segment color='orange'> {speedObj.value} </Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'positive') {
      return (
        <Table.Cell positive>
          <Segment color='green'> {speedObj.value}</Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'negative') {
      return (
        <Table.Cell negative>
          <Segment color='red'> {speedObj.value}</Segment>
        </Table.Cell>
      );
    } else {
      return (
        <Table.Cell>
          <Segment> {speedObj.value}</Segment>
        </Table.Cell>
      );
    }
  }

  addRowsDynamically() {
    return this.props.liveHealthTable.map((vessel, index) => {
      return (
        <Table.Row key={index}>
          <Table.Cell>
            <Button basic fluid>
              {vessel.vesselname}
            </Button>
          </Table.Cell>
          <Table.Cell>
            <Button basic fluid>
              {vessel.servicename}
            </Button>
          </Table.Cell>
          <Table.Cell>
            <Button
              basic
              fluid
              onClick={() => {
                this.props.history.push({ pathname: '/fleetmap' });
                this.props.dispatch({
                  type: 'TAB_CHANGED',
                  payload: 'fleetmap'
                });
                this.props.dispatch({
                  type: 'CHECKBOX_STATE_CHANGED',
                  payload: false
                });
                this.props.dispatch({
                  type: 'LIST_CHECKBOX_STATUS_CHANGED',
                  payload: vessel.vesselcode
                });
              }}
            >
              {vessel.flag === 0 ? (
                <Icon name='attention' color='green' />
              ) : null}
              {vessel.flag === 1 ? (
                <Icon name='attention' color='orange' />
              ) : null}
              {vessel.flag === 2 ? <Icon name='attention' color='red' /> : null}
              {vessel.vesselcode}
            </Button>
          </Table.Cell>
          {this.addCell(vessel.speed)}
          {this.addCell(vessel['route-deviation'])}
          {this.addCell(vessel.weather)}
          {/* {this.addCell(vessel.bunker)} */}
          {this.addCell(vessel['next-port'])}
          {this.addCell(vessel.deviation)}
          {this.addCell(vessel.distance)}
          {this.addCell(vessel.lastupdate)}
        </Table.Row>
      );
    });
  }

  render() {
    return (
      <div>
        <br />
        <Divider />
        <br />
        <Grid columns={3}>
          {/* <Grid.Column width={1}>
                    </Grid.Column> */}
          <Grid.Column width={16}>
            <Table celled className='tripHistoryTable'>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={2}>Vessel Name</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Service Name</Table.HeaderCell>
                  <Table.HeaderCell width={2}>Voyage Number</Table.HeaderCell>
                  <Table.HeaderCell width={2}>
                    Speed Report (Knots)
                  </Table.HeaderCell>
                  <Table.HeaderCell width={2}>
                    Route Deviation (NM)
                  </Table.HeaderCell>
                  <Table.HeaderCell width={1}>Weather (1- 10)</Table.HeaderCell>

                  {/* <Table.HeaderCell width={2}>Bunker (Tonnes) </Table.HeaderCell> */}
                  <Table.HeaderCell width={2}>Next Port ETA </Table.HeaderCell>
                  <Table.HeaderCell width={1}>
                    {' '}
                    ETA Deviation (Hrs)
                  </Table.HeaderCell>
                  <Table.HeaderCell width={1}>
                    {' '}
                    Distance Travelled (NM)
                  </Table.HeaderCell>
                  <Table.HeaderCell width={2}>
                    {' '}
                    Last Reported Time
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{this.addRowsDynamically()}</Table.Body>
            </Table>
          </Grid.Column>
          {/* <Grid.Column width={1}>
                    </Grid.Column> */}
        </Grid>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    authData: globalData.authData,
    tabData: globalData.tabData,
    liveHealthTable: globalData.liveHealthData.liveHealthTable
  };
}

const myConnector = connect(mapStatesToProps);
export default withRouter(myConnector(LiveHealth));
