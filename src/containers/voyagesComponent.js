/* global google */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Divider, Button, Table } from 'semantic-ui-react';
import moment from 'moment';
import {
  fetchVoyageLocations,
  fetchAllVoyages
} from '../actions/voyageActions';
import MapView from '../components/voyageMapComponent';

const getFormattedDate = function(datetime) {
  return moment(datetime).format('YYYY-MM-DD');
};

const getFormattedTime = function(datetime) {
  return moment(datetime).format('HH:mm:ss');
};

class ShipVoyages extends Component {
  constructor(props) {
    super(props);
    this.tripClicked = this.tripClicked.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(fetchAllVoyages());
  }

  tripClicked(id, tripScore) {
    console.log('trip clicked', id);
    // this.props.dispatch(fetchTripLocationData(this.props.authData.token, id));
    // this.props.dispatch({ type: "SHOW_HIDE_TRIP_EVENTS_BUTTON", payload: true });
    // this.props.dispatch({ type: "SELECTED_TRIP_ID_IN_TRIP_PAGE", payload:{ tripId :id, tripScore:tripScore} });
    // this.props.dispatch(getScoreBreakdown(this.props.authData.token, id))
    this.props.dispatch(fetchVoyageLocations(id));

    // window.scrollTo(0, 0);
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 1000);
  }

  // AddPageNumbers() {
  //     return <Table.Footer>
  //         <Table.Row key={10000000}>
  //             <Table.HeaderCell colSpan='10'>
  //                 {/*You can change the css by looking into the corresponding classes in the css file */}
  //                 <ReactPaginate previousLabel={"previous"}
  //                     nextLabel={"next"}
  //                     breakLabel={<a href="">...</a>}
  //                     breakClassName={"BreakView"}
  //                     pageCount={Math.ceil((this.props.totalTrips) / 10)}
  //                     initialPage={this.props.tripsPage}
  //                     forcePage={this.props.tripsPage}
  //                     marginPagesDisplayed={2}
  //                     pageRangeDisplayed={5}
  //                     onPageChange={
  //                         (data) => {
  //                             this.props.dispatch(fetchDriverTrips(this.props.authData.token, this.props.selectedDriver, data.selected));
  //                         }}
  //                     containerClassName={"pagination"}
  //                     subContainerClassName={"paginationPage"}
  //                     activeClassName={"active"} />
  //             </Table.HeaderCell>
  //         </Table.Row>
  //     </Table.Footer>
  // }

  addTripRowsDynamically() {
    if (this.props.allVoyages.length !== 0) {
      if (true) {
        return this.props.allVoyages.map((voyage, index) => {
          return (
            <Table.Row key={index}>
              <Table.Cell>
                {
                  <Button onClick={this.tripClicked.bind(this, voyage.id, 100)}>
                    {voyage.id}
                  </Button>
                }
              </Table.Cell>
              <Table.Cell>{voyage.vesselName}</Table.Cell>
              <Table.Cell>{voyage.startPort}</Table.Cell>
              <Table.Cell>
                {getFormattedDate(voyage.startTime)}
                <br />
                {getFormattedTime(voyage.startTime)}
              </Table.Cell>
              <Table.Cell>
                {getFormattedDate(voyage.endTime)}
                <br />
                {getFormattedTime(voyage.endTime)}
              </Table.Cell>

              {/* <Table.Cell>{getFormattedDate(trip.tripEndtime)}
                                <br />
                                {getFormattedTime(trip.tripEndtime)}
                            </Table.Cell> */}
              <Table.Cell>{voyage.netSpeed}</Table.Cell>
              <Table.Cell>{voyage.seaTime}</Table.Cell>
              <Table.Cell>{voyage.portTime}</Table.Cell>
              <Table.Cell>{voyage.totalDistance}</Table.Cell>

              <Table.Cell>{voyage.totalPortCalls}</Table.Cell>
            </Table.Row>
          );
        });
      }
      // else {
      //     this.props.dispatch(fetchAllVoyages());
      // }
    } else {
      return (
        <Table.Row>
          <Table.Cell>No Voyages Found.</Table.Cell>
        </Table.Row>
      );
    }
  }

  render() {
    return (
      <div>
        <br />
        <Divider />
        <MapView
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={
            <div
              style={{ height: `100%`, marginLeft: '1%', marginRight: '1%' }}
            />
          }
          loadingElement={<div style={{ height: `100%` }} />}
        />
        <Divider />
        <br />
        <br />
        <div>
          <Table celled className='tripHistoryTable'>
            <Table.Header>
              <Table.Row>
                {/* <Table.HeaderCell>Driver </Table.HeaderCell> */}
                <Table.HeaderCell>Voyage ID</Table.HeaderCell>
                <Table.HeaderCell>Vessel Name</Table.HeaderCell>
                <Table.HeaderCell>Start Port</Table.HeaderCell>
                <Table.HeaderCell>Start Time</Table.HeaderCell>
                {/* <Table.HeaderCell>End Time</Table.HeaderCell> */}
                <Table.HeaderCell>End Time</Table.HeaderCell>
                <Table.HeaderCell>Net Speed</Table.HeaderCell>
                <Table.HeaderCell>Sea Time [Days] </Table.HeaderCell>
                <Table.HeaderCell>Port Time [Hours]</Table.HeaderCell>
                <Table.HeaderCell>Total Distance</Table.HeaderCell>
                <Table.HeaderCell>Total Port Calls</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.addTripRowsDynamically()}</Table.Body>
            {/* {(Math.ceil(this.props.totalTrips / 10) > 1.0) ? this.AddPageNumbers() : null} */}
          </Table>
        </div>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  // console.log("global data trip details", globalData)
  return {
    authData: globalData.authData,
    tabData: globalData.tabData,
    allVoyages: globalData.voyagesData.allVoyages,
    voyageLocations: globalData.voyagesData.voyageLocations,
    mainMapCenter: globalData.mapSearchData.mainMapCenter
  };
}

const myConnector = connect(mapStatesToProps);
export default myConnector(ShipVoyages);
