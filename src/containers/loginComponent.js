import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, Divider } from 'semantic-ui-react';
import { authenticateUser } from '../actions/loginActions';
import "../styles/login.css";
// import { instanceOf } from 'prop-types';
// import { withCookies, Cookies } from 'react-cookie';  

//import cookie from 'react-cookies'

class Login extends Component {
    constructor(props) {
        super(props);
        //this.state = { token: cookie.load('token') };
        //this.state = { userName: cookie.load('userName'), userPassword: cookie.load('userPassword) };
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
        this.handleUsernameChange  = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }    
    handleLoginSubmit(e) {
        // console.log("submitted.",this.props.authData);
        //this.setState({ token })
        //cookie.save('token', this.props.authData.token, { path: '/' })
        return this.props.dispatch(authenticateUser(this.props.authData.userName, this.props.authData.userPassword,this.props.authData.token));
    }

    handleUsernameChange(e) {
        // console.log("USER_NAME_CHANGED", e.target.value)
        this.props.dispatch({ type: "USER_NAME_CHANGED", payload: e.target.value });
    }

    handlePasswordChange(e) {
        // console.log("USER_PASSWORD_CHANGED", e.target.value)
        this.props.dispatch({ type: "USER_PASSWORD_CHANGED", payload: e.target.value });
    }

    render() {
        var invalidCredentials;
        if(this.props.loginSuccess===false){
            invalidCredentials =<div className="invalidCredential"><b>{this.props.loginMessage}</b></div>
        }
        else{
            invalidCredentials = null
        }

        return (
            <div >
              <br/>
              <br/>
                <center>
                    <font size="+1">User Login</font>
                </center>
                <Divider />
                <Form   onSubmit={this.handleLoginSubmit} className="loginComponentForm">
                    <Form.Field>
                        <label>User Name</label>
                        <input placeholder='Name' onChange={this.handleUsernameChange}
                        value={this.props.authData.userName}/>
                    </Form.Field>
                    <Form.Field>
                        <label>Your Password</label>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            value={this.props.authData.userPassword}
                            onChange={this.handlePasswordChange}
                        />
                    </Form.Field>
                    <Button basic type='submit'
                        color='purple'
                    >
                        Log In
                    </Button>
                    { invalidCredentials }
                </Form>
                <Divider />
            </div>
        )
    }
}



// class Login extends Component {
//   render() {
//     return (
//       <div>
//         <br />
//         <br />
//         Login 
//       </div>
//     );
//   }
// }

function mapStatesToProps(globalData) {
  // console.log(globalData)
  return {
      authData: globalData.authData,
      loginMessage: globalData.authData.loginMessage,
      loginSuccess: globalData.authData.loginSuccess,
      
  };
}

const myConnector = connect(mapStatesToProps);
export default myConnector(Login);