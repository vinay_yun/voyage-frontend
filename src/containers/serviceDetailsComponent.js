import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Divider,
  Dropdown,
  Grid,
  Table,
  Button,
  Segment,
  Icon
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import '../styles/serviceReportContainer.css';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class VoyageDetails extends Component {
  addCell(speedObj, refTime) {
    if (speedObj.status === 'normal') {
      return (
        <Table.Cell>
          <Segment color='orange'>
            {' '}
            {speedObj.value} <br /> {refTime}
          </Segment>{' '}
        </Table.Cell>
      );
    } else if (speedObj.status === 'positive') {
      return (
        <Table.Cell positive>
          <Segment color='green'>
            {' '}
            {speedObj.value} <br /> {refTime}
          </Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'negative') {
      return (
        <Table.Cell negative>
          <Segment color='red'>
            {' '}
            {speedObj.value} <br /> {refTime}
          </Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'na') {
      return (<Table.Cell>
        <Segment>
          {' '}
          {speedObj.value} <br /> {refTime}
        </Segment>
      </Table.Cell>
      );
    }
  }

  addColoredRowsFinalDynamically() {
    return this.props.serviceDetailsColoredFinal[
      this.props.serviceNumberSelected
    ].map((leg, index) => {
      return (
        <Table.Row key={100 + index}>
          <Table.Cell>{leg.legID}</Table.Cell>
          <Table.Cell>{leg.portCall}</Table.Cell>
          {this.addCell(leg.arrivalTime, leg.estiArrivalTime)}
          {this.addCell(leg.departureTime, leg.EstiDepartureTime)}
          {this.addCell(leg.netSpeed, leg.estiNetSpeed)}
          {this.addCell(leg.distanceTravelled, leg.estiDistaneTravelled)}
          {this.addCell(leg.portTime, leg.estiPortTime)}
          {this.addCell(leg.seaTime, leg.estiSeaTime)}
          {this.addCell(leg.mvsPerHour, leg.estiMvsPerHOur)}
          {this.addCell(leg.totalMoves, leg.estiTotalMoves)}
          {this.addCell(leg.bunkerUsage, leg.estiBunkerUsage)}
          <Table.Cell>
            {leg.idleTime} <br /> {0}
          </Table.Cell>
        </Table.Row>
      );
    });
  }

  addColoredRowsDynamically() {
    return this.props.serviceDetailsColored.map((leg, index) => {
      return (
        <Table.Row key={100 + index}>
          <Table.Cell>{leg.legID}</Table.Cell>
          <Table.Cell>{leg.portCall}</Table.Cell>
          {this.addCell(leg.arrivalTime, leg.estiArrivalTime)}
          {this.addCell(leg.departureTime, leg.EstiDepartureTime)}
          {this.addCell(leg.netSpeed, leg.estiNetSpeed)}
          {this.addCell(leg.distanceTravelled, leg.estiDistaneTravelled)}
          {this.addCell(leg.portTime, leg.estiPortTime)}
          {this.addCell(leg.seaTime, leg.estiSeaTime)}
          {this.addCell(leg.mvsPerHour, leg.estiMvsPerHOur)}
          {this.addCell(leg.totalMoves, leg.estiTotalMoves)}
          {this.addCell(leg.bunkerUsage, leg.estiBunkerUsage)}
          <Table.Cell>
            {leg.idleTime} <br /> {0}
          </Table.Cell>
        </Table.Row>
      );
    });
  }
  addRowsDynamically() {
    return this.props.serviceDetails.map((leg, index) => {
      return (
        <Table.Row key={100 + index}>
          <Table.Cell>{leg.legID}</Table.Cell>
          <Table.Cell>{leg.portCall}</Table.Cell>
          <Table.Cell>
            {leg.arrivalTime} <br /> {leg.estiArrivalTime}
          </Table.Cell>
          <Table.Cell>
            {leg.departureTime} <br /> {leg.EstiDepartureTime}
          </Table.Cell>
          <Table.Cell>
            {leg.netSpeed} <br /> {leg.estiNetSpeed}
          </Table.Cell>
          <Table.Cell>
            {leg.distanceTravelled} <br /> {leg.estiDistaneTravelled}
          </Table.Cell>
          <Table.Cell>
            {leg.portTime} <br /> {leg.estiPortTime}
          </Table.Cell>
          <Table.Cell>
            {leg.seaTime} <br /> {leg.estiSeaTime}
          </Table.Cell>
          <Table.Cell>
            {leg.mvsPerHour} <br /> {leg.estiMvsPerHOur}
          </Table.Cell>
          <Table.Cell>
            {leg.totalMoves} <br /> {leg.estiTotalMoves}
          </Table.Cell>
          <Table.Cell>
            {leg.bunkerUsage} <br /> {leg.estiBunkerUsage}
          </Table.Cell>
          <Table.Cell>
            {leg.idleTime} <br /> {0}
          </Table.Cell>

          {/* <Table.HeaderCell width={1}>Arrival Time</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Departure Time</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Net Speed</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Distance</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Port Time</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Sea Time</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Moves/Hr</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Total Moves</Table.HeaderCell>
                    <Table.HeaderCell width={1}>Bunker</Table.HeaderCell> */}
        </Table.Row>
      );
    });
  }

  render() {
    return (
      <div>
        <br />
        <Button
          onClick={() => {
            this.props.dispatch({ type: 'GO_BACK_TO_SERVICES' });
          }}
        >
          <Icon
            name='long arrow alternate left'
            onClick={() => {
              this.props.dispatch({ type: 'GO_BACK_TO_SERVICES' });
            }}
          />
        </Button>
        <Divider />
        <Grid columns={5}>
          <Grid.Row>
            <Grid.Column />
            <Grid.Column>
              Service Name: {this.props.serviceNameSelected}
            </Grid.Column>
            <Grid.Column>
              Voyage Number: {this.props.serviceNumberSelected}
            </Grid.Column>
            <Grid.Column>
              Vessel Name: {this.props.vesselNameSelected}
            </Grid.Column>
            <Grid.Column />
          </Grid.Row>
        </Grid>
        <Divider />
        <Table celled className='tripHistoryTable'>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={1}>Leg ID</Table.HeaderCell>
              <Table.HeaderCell width={1}>Port Call</Table.HeaderCell>
              <Table.HeaderCell width={2}>Arrival Time</Table.HeaderCell>
              <Table.HeaderCell width={2}>Departure Time</Table.HeaderCell>
              <Table.HeaderCell width={1}>Net Speed</Table.HeaderCell>
              <Table.HeaderCell width={1}>Distance</Table.HeaderCell>
              <Table.HeaderCell width={1}>Port Time</Table.HeaderCell>
              <Table.HeaderCell width={1}>Sea Time</Table.HeaderCell>
              <Table.HeaderCell width={1}>Moves/Hr</Table.HeaderCell>
              <Table.HeaderCell width={1}>Total Moves</Table.HeaderCell>
              <Table.HeaderCell width={1}>Bunker</Table.HeaderCell>
              <Table.HeaderCell width={1}>Idle Time (Hrs)</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {/* {this.addRowsDynamically()} */}
            {/* {this.addColoredRowsDynamically()} */}
            {this.addColoredRowsFinalDynamically()}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

class ServiceDetails extends Component {

  serviceChanged(event, data) {
    console.log('Service Selected', data.value);
    this.props.dispatch({ type: 'SERVICE_CHANGED', payload: data.value });
  }

  addCell(speedObj) {
    if (speedObj.status === 'normal') {
      return (
        <Table.Cell>
          <Segment color='orange'> {speedObj.value} </Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'positive') {
      return (
        <Table.Cell positive>
          <Segment color='green'> {speedObj.value}</Segment>
        </Table.Cell>
      );
    } else if (speedObj.status === 'negative') {
      return (
        <Table.Cell negative>
          <Segment color='red'> {speedObj.value}</Segment>
        </Table.Cell>
      );
    }
  }
  // addCell(speedObj) {
  //     if (speedObj.status === 'normal') {
  //         return (
  //             <Table.Cell><Segment inverted color='orange'> {speedObj.value} </Segment></Table.Cell>
  //         )
  //     } else if (speedObj.status === 'positive') {
  //         return (
  //             <Table.Cell positive><Segment inverted color='green'> {speedObj.value}</Segment></Table.Cell>

  //         )
  //     } else if (speedObj.status === 'negative') {
  //         return (
  //             <Table.Cell negative><Segment inverted color='red'> {speedObj.value}</Segment></Table.Cell>
  //         )
  //     }
  // }

  addRowsDynamically() {
    return this.props.servicesTable.map((journey, index) => {
      if (
        this.props.serviceSelected.length > 0 &&
        !(this.props.serviceSelected === 'Select Service')
      ) {
        if (this.props.serviceSelected === journey.serviceName) {
          return (
            <Table.Row key={index}>
              <Table.Cell>
                <Button basic fluid>
                  {' '}
                  {journey.serviceName}
                </Button>
              </Table.Cell>
              <Table.Cell>
                <Button
                  basic
                  fluid
                  onClick={() => {
                    this.props.dispatch({
                      type: 'SERVICE_SELECTED',
                      payload: {
                        serviceName: journey.serviceName,
                        voyageNumber: journey.serviceNumber,
                        vesselName: journey.vesselName
                      }
                    });
                  }}
                >
                  {journey.flag ? <Icon name='attention' color='red' /> : null}{' '}
                  {journey.serviceNumber}
                </Button>
              </Table.Cell>
              <Table.Cell>
                <Button basic fluid>
                  {journey.vesselName}
                </Button>
              </Table.Cell>
              {/* {this.addCell(journey.startDate)}
                            {this.addCell(journey.endDate)} */}
              {this.addCell(journey.distanceTravelled)}
              {this.addCell(journey.bunkerUsage)}
              {this.addCell(journey.netSpeed)}
              {this.addCell(journey.seaTime)}
              {this.addCell(journey.portTime)}
              {this.addCell(journey.totalMoves)}
              {this.addCell(journey.movesPerHour)}
              {this.addCell(journey.weather)}
            </Table.Row>
          );
        } else {
          return null;
        }
      } else {
        return (
          <Table.Row key={index}>
            <Table.Cell>
              <Button basic fluid>
                {journey.serviceName}
              </Button>
            </Table.Cell>
            <Table.Cell>
              <Button
                basic
                fluid
                onClick={() => {
                  this.props.dispatch({
                    type: 'SERVICE_SELECTED',
                    payload: {
                      serviceName: journey.serviceName,
                      voyageNumber: journey.serviceNumber,
                      vesselName: journey.vesselName
                    }
                  });
                }}
              >
                {journey.flag === 0 ? (
                  <Icon name='attention' color='green' />
                ) : null}
                {journey.flag === 1 ? (
                  <Icon name='attention' color='orange' />
                ) : null}{' '}
                {journey.flag === 2 ? (
                  <Icon name='attention' color='red' />
                ) : null}
                {journey.serviceNumber}
              </Button>
            </Table.Cell>
            <Table.Cell>
              <Button basic fluid>
                {journey.vesselName}
              </Button>
            </Table.Cell>
            {/* {this.addCell(journey.startDate)}
                        {this.addCell(journey.endDate)} */}
            {this.addCell(journey.distanceTravelled)}
            {this.addCell(journey.bunkerUsage)}
            {this.addCell(journey.netSpeed)}
            {this.addCell(journey.seaTime)}
            {this.addCell(journey.portTime)}
            {this.addCell(journey.totalMoves)}
            {this.addCell(journey.movesPerHour)}
            {this.addCell(journey.weather)}
          </Table.Row>
        );
      }
    });
  }

  render() {
    const serviceOptions = [
      { text: 'TPH', value: 'TPH', key: 1 },
      { text: 'SVC', value: 'SVC', key: 2 },
      { text: 'SLC', value: 'SLC', key: 3 },
      { text: 'Select Service', value: 'Select Service', key: 4 }
    ];
    return (
      <div>
        <br />
        <Divider />
        <Dropdown
          placeholder='Select Service'
          selection
          className='serviceSelection'
          options={serviceOptions}
          onChange={this.serviceChanged.bind(this)}
          style={{ marginLeft: '5%' }}
        />

        <div className='datepickerClass'>
          <DatePicker
            className={'selectServicDatepickerEndDate'}
            dateFormat='DD-MM-YYYY'
            selected={new moment('2018-07-01')}
            selectsEnd
            startDate={this.props.selectServiceStartDate}
            endDate={this.props.selectServiceEndDate}
            onChange={this.handleChangeEndDate}
          />
          <DatePicker
            className={'selectServicDatepickerStartDate'}
            dateFormat='DD-MM-YYYY'
            selected={new moment('2018-08-01')}
            selectsStart
            startDate={new moment()}
            endDate={new moment('2018-09-20')}
            onChange={this.handleChangeStartDate}
          />
        </div>
        <br />
        <Divider />
        <Grid columns={1}>
          {/* <Grid.Column width={1}>
                    </Grid.Column> */}
          <Grid.Column width={16}>
            <Table celled className='tripHistoryTable'>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={1}>Service Name</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Voyage Number</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Vessel Name</Table.HeaderCell>
                  {/* <Table.HeaderCell width={1}>Start Date</Table.HeaderCell>
                                    <Table.HeaderCell width={1}>End Date</Table.HeaderCell> */}
                  <Table.HeaderCell width={1}>Distance (NM)</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Bunker (Tonnes)</Table.HeaderCell>
                  <Table.HeaderCell width={1}>
                    Net Speed (Knots){' '}
                  </Table.HeaderCell>
                  <Table.HeaderCell width={1}>Sea Time (Hrs) </Table.HeaderCell>
                  <Table.HeaderCell width={1}>
                    Port Time (Hrs){' '}
                  </Table.HeaderCell>
                  <Table.HeaderCell width={1}>Total Moves</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Moves/Hr (Hrs) </Table.HeaderCell>
                  <Table.HeaderCell width={1}>Weather </Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>{this.addRowsDynamically()}</Table.Body>
            </Table>
          </Grid.Column>
          {/* <Grid.Column width={1}>
                    </Grid.Column> */}
        </Grid>
      </div>
    );
  }
}

class ServicesComponent extends Component {
  render() {
    return (
      <div>
        {this.props.showVoyageDetails ? (
          <VoyageDetailsConnected />
        ) : (
          <ServiceDetailsConnected />
        )}
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    authData: globalData.authData,
    tabData: globalData.tabData,
    servicesTable: globalData.servicesData.servicesTable,
    serviceSelected: globalData.servicesData.serviceSelected,
    serviceNameSelected: globalData.servicesData.serviceNameSelected,
    serviceNumberSelected: globalData.servicesData.serviceNumberSelected,
    vesselNameSelected: globalData.servicesData.vesselNameSelected,
    showVoyageDetails: globalData.servicesData.showVoyageDetails,
    serviceDetails: globalData.servicesData.serviceDetails,
    serviceDetailsColored: globalData.servicesData.serviceDetailsColored,
    serviceDetailsColoredFinal:
      globalData.servicesData.serviceDetailsColoredFinal
  };
}

const myConnector = connect(mapStatesToProps);
const ServiceDetailsConnected = myConnector(ServiceDetails);
const VoyageDetailsConnected = myConnector(VoyageDetails);
export default myConnector(ServicesComponent);
