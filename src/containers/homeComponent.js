import React, { Component } from 'react';
import { connect } from 'react-redux';
import SeachList from '../components/searchListComponent';
import MapView from '../components/mapComponent';
import { Grid } from 'semantic-ui-react';
import '../styles/searchListComponent.css';
import { Table } from 'semantic-ui-react';

class LiveVoyageDetails extends Component {
  addRowsDynamically() {
    return this.props.voyageDetailsNew[this.props.deviceIDSelected].map(
      (leg, index) => {
        return (
          <Table.Row key={100 + index}>
            <Table.Cell textAlign='center'>{leg.legID}</Table.Cell>
            <Table.Cell>{leg.portCall}</Table.Cell>
            <Table.Cell>
              {leg.arrivalTime} <br /> {leg.estiArrivalTime}
            </Table.Cell>
            <Table.Cell>
              {leg.departureTime} <br /> {leg.EstiDepartureTime}
            </Table.Cell>
            <Table.Cell>
              {leg.netSpeed} <br /> {leg.estiNetSpeed}
            </Table.Cell>
            <Table.Cell>
              {leg.distanceTravelled} <br /> {leg.estiDistaneTravelled}
            </Table.Cell>
            <Table.Cell>
              {leg.portTime} <br /> {leg.estiPortTime}
            </Table.Cell>
            <Table.Cell>
              {leg.seaTime} <br /> {leg.estiSeaTime}
            </Table.Cell>
            <Table.Cell>
              {leg.mvsPerHour} <br /> {leg.estiMvsPerHOur}
            </Table.Cell>
            <Table.Cell>
              {leg.totalMoves} <br /> {leg.estiTotalMoves}
            </Table.Cell>
            <Table.Cell>
              {leg.bunkerUsage} <br /> {leg.estiBunkerUsage}
            </Table.Cell>
            <Table.Cell>
              {leg.idleTime} <br /> {0}
            </Table.Cell>
          </Table.Row>
        );
      }
    );
  }

  render() {
    return (
      <Table celled className='tripHistoryTable'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell textAlign='center' width={1}>
              Leg ID
            </Table.HeaderCell>
            <Table.HeaderCell width={1}>Port Call</Table.HeaderCell>
            <Table.HeaderCell width={2}>Arrival Time</Table.HeaderCell>
            <Table.HeaderCell width={2}>Departure Time</Table.HeaderCell>
            <Table.HeaderCell width={1}>Net Speed</Table.HeaderCell>
            <Table.HeaderCell width={1}>Distance</Table.HeaderCell>
            <Table.HeaderCell width={1}>Port Time</Table.HeaderCell>
            <Table.HeaderCell width={1}>Sea Time</Table.HeaderCell>
            <Table.HeaderCell width={1}>Moves/Hr</Table.HeaderCell>
            <Table.HeaderCell width={1}>Total Moves</Table.HeaderCell>
            <Table.HeaderCell width={1}>Bunker</Table.HeaderCell>
            <Table.HeaderCell width={1}>Idle Time (Hrs)</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.addRowsDynamically()}</Table.Body>
      </Table>
    );
  }
}

class Home extends Component {
  render() {
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column
              className='searchList'
              mobile={16}
              tablet={4}
              computer={4}
            >
              <SeachList />
            </Grid.Column>
            <Grid.Column
              className='mapView'
              mobile={16}
              tablet={12}
              computer={12}
            >
              <br />
              <MapView
                containerElement={<div style={{ height: `450px` }} />}
                mapElement={
                  <div
                    style={{
                      height: `100%`,
                      marginLeft: '1%',
                      marginRight: '1%'
                    }}
                  />
                }
                loadingElement={<div style={{ height: `100%` }} />}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <LiveVoyageDetailsConnected />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    // serviceDetails: globalData.servicesData.serviceDetails
    voyageDetails: globalData.mapSearchData.voyageDetails,
    voyageDetailsNew: globalData.mapSearchData.voyageDetailsNew,
    deviceIDSelected: globalData.mapSearchData.deviceIDSelected
  };
}

const myConnector = connect(mapStatesToProps);
const LiveVoyageDetailsConnected = myConnector(LiveVoyageDetails);
export default myConnector(Home);
