import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Divider,
  Dropdown,
  Grid,
  Label
} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import '../styles/serviceReportContainer.css';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Pie,
  PieChart,
  Cell
} from 'recharts';

const COLORS = [
  '#0067CE',
  '#00E49F',
  '#FFBB28',
  '#FF8042',
  '#0088FE',
  '#00C47F',
  '#CCBB28',
  '#CC8042',
  '#0088FE',
  '#00C49F',
  '#FFBB28',
  '#FF8042'
];
const WEATHERCOLORS = [
  '#00FF00',
  '#33FF00',
  '#77FF00',
  '#AAFF00',
  '#FFCC00',
  '#FFAA00',
  '#FF8800',
  '#FF6600',
  '#FF3300',
  '#FF0000'
];
const PERCENTCOLORS = ['#FF6633', '#66FF33'];
class FleetAnalytics extends Component {

  serviceChanged(event, data) {
    console.log('Service Selected', data.value);
    this.props.dispatch({
      type: 'ANALYTICS_SERVICE_CHANGED',
      payload: data.value
    });
  }

  render() {
    const serviceOptions = [
      { text: 'TPH', value: 'TPH', key: 1 },
      { text: 'SVC', value: 'SVC', key: 2 },
      { text: 'SLC', value: 'SLC', key: 3 },
      { text: 'Select Service', value: 'Select Service', key: 4 }
    ];
    return (
      <div>
        <br />
        <Divider />
        <Dropdown
          placeholder='Select Service'
          selection
          className='serviceSelection'
          options={serviceOptions}
          onChange={this.serviceChanged.bind(this)}
          style={{ marginLeft: '5%' }}
        />

        <div className='datepickerClass'>
          <DatePicker
            className={'selectServicDatepickerEndDate'}
            dateFormat='DD-MM-YYYY'
            selected={new moment('2018-07-01')}
            selectsEnd
            startDate={this.props.selectServiceStartDate}
            endDate={this.props.selectServiceEndDate}
            onChange={this.handleChangeEndDate}
          />
          <DatePicker
            className={'selectServicDatepickerStartDate'}
            dateFormat='DD-MM-YYYY'
            selected={new moment('2018-08-01')}
            selectsStart
            startDate={new moment('2018-08-01')}
            endDate={new moment('2018-08-02')}
            onChange={this.handleChangeStartDate}
          />
        </div>
        <br />
        <br />
        <Grid>
          <Grid.Column width={4} />
          <Grid.Column width={10}>
            <h2>Bunker Consumption(Tonnes) by Services</h2>
            <BarChart
              width={800}
              height={400}
              data={this.props.bunkerConsumption}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis
                label={{ value: 'Tonnes', angle: -90, position: 'insideLeft' }}
              />
              <Tooltip />
              <Legend />
              <Bar dataKey='totalConsumed' fill='#8884d8' />
              <Bar dataKey='expectedConsumption' fill='#82ca9d' />
            </BarChart>
            <br />
            <h2>Bunker Consumption(Kgs per NM) by Services</h2>
            <BarChart
              width={800}
              height={400}
              data={this.props.bunkerConsumption}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis
                label={{ value: 'Kg/NM', angle: -90, position: 'insideLeft' }}
              />
              <Tooltip />
              <Legend />
              <Bar dataKey='bunkerConsumedKgPerNM' fill='#8884d8' />
            </BarChart>
            <br />
            <h2>Bunker Consumption(Kgs per TEU) by Services</h2>
            <BarChart
              width={800}
              height={400}
              data={this.props.bunkerConsumption}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis
                label={{ value: 'Kg/TEU', angle: -90, position: 'insideLeft' }}
              />
              <Tooltip />
              <Legend />
              <Bar dataKey='bunkerConsumedKgPerContainer' fill='#82ca9d' />
            </BarChart>
            <br />
            <h2>Portcall Reliability Report -ETA Deviation</h2>
            <PieChart
              width={600}
              height={400}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              {/* <CartesianGrid strokeDasharray="3 3" /> */}
              {/* <XAxis dataKey="name" />
                            <YAxis /> */}
              <Tooltip />
              <Legend align='left' layout='vertical' verticalAlign='middle' />
              <Pie
                data={this.props.reliabilityReport.timeDelaysPie}
                fill='#FF3300'
                label
              >
                {this.props.reliabilityReport.timeDelaysPie.map(
                  (entry, index) => (
                    <Cell fill={COLORS[index % COLORS.length]} />
                  )
                )}
              </Pie>
              {/* <Bar dataKey="expectedConsumption" fill="#82ca9d" /> */}
            </PieChart>
            {/* <BarChart width={800} height={400} data={this.props.reliabilityReport.timeDelays}
                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="vessels" fill="#FF3300" />
                        </BarChart> */}

            <br />
            <h3>Avg Hours Delays from Scheduled </h3>
            <BarChart
              width={800}
              height={400}
              data={this.props.reliabilityReport.serviceDelays}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis
                label={{ value: 'Hours', angle: -90, position: 'insideLeft' }}
              />
              <Tooltip />
              <Legend />
              <Bar dataKey='hours' fill='#FF3300' />
              {/* <Bar dataKey="expectedConsumption" fill="#82ca9d" /> */}
            </BarChart>
            <br />
            <h3>Avg Hours Early Arrival from Scheduled </h3>
            <BarChart
              width={800}
              height={400}
              data={this.props.reliabilityReport.serviceEarly}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis
                label={{ value: 'Hours', angle: -90, position: 'insideLeft' }}
              />
              <Tooltip />
              <Legend />
              <Bar dataKey='hours' fill='#FF3300' />
              {/* <Bar dataKey="expectedConsumption" fill="#82ca9d" /> */}
            </BarChart>
            <br />
            <h2>Weather Report</h2>
            <Grid columns={2}>
              <Grid.Column>
                <h3>Percentage of Services Affected</h3>
                <PieChart width={400} height={400}>
                  <Tooltip />
                  <Pie
                    data={this.props.weatherReport.percentImpacts}
                    dataKey='value'
                    nameKey='name'
                    cx='50%'
                    cy='50%'
                    fill='#DD3366'
                    labelLine={false}
                    label
                  >
                    {' '}
                    <Label value='name' position='inside' />
                    {this.props.weatherReport.percentImpacts.map(
                      (entry, index) => (
                        <Cell
                          fill={PERCENTCOLORS[index % PERCENTCOLORS.length]}
                        />
                      )
                    )}
                  </Pie>
                </PieChart>
              </Grid.Column>
              <Grid.Column>
                <h3>Hours and Weather level Distribution</h3>
                <PieChart width={400} height={400}>
                  <Tooltip />
                  <Pie
                    data={this.props.weatherReportWithLevels}
                    dataKey='value'
                    nameKey='name'
                    cx='50%'
                    cy='50%'
                    fill='#8884d8'
                    label
                    labelLine={false}
                  >
                    {this.props.weatherReportWithLevels.map((entry, index) => (
                      <Cell
                        fill={WEATHERCOLORS[index % WEATHERCOLORS.length]}
                      />
                    ))}
                  </Pie>
                </PieChart>
              </Grid.Column>
            </Grid>
          </Grid.Column>
          <Grid.Column width={3} />
        </Grid>
      </div>
    );
  }
}

function mapStatesToProps(globalData) {
  return {
    authData: globalData.authData,
    tabData: globalData.tabData,
    serviceSelected: globalData.analyticsData.serviceSelected,
    bunkerConsumption: globalData.analyticsData.bunkerConsumption,
    reliabilityReport: globalData.analyticsData.reliabilityReport,
    weatherReport: globalData.analyticsData.weatherReport,
    weatherReportWithLevels: globalData.analyticsData.weatherReportWithLevels
  };
}

const myConnector = connect(mapStatesToProps);
export default myConnector(FleetAnalytics);
