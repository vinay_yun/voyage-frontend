import axios from 'axios';
import { api_base_url } from '../config';

export function fetchAllVoyages() {
  console.log('Fetching Voyages.');
  return function(dispatch) {
    axios
      .get(api_base_url + '/voyages')
      .then(response => {
        console.log('fetched.', response.data);
        if (Array.isArray(response.data)) {
          dispatch({
            type: 'ALL_VOYAGES_RECEIVED',
            payload: response.data
          });
        } else if (!Array.isArray(response.data)) {
          dispatch({
            type: 'ALL_VOYAGES_FAILED',
            payload: response.data
          });
        }
      })
      .catch(err => {
        console.log('fetched.', err);
        dispatch({
          type: 'ALL_VOYAGES_FAILED',
          payload: err
        });
      });
  };
}

export function fetchVoyageLocations(id) {
  console.log('Fetching Voyage locations.');
  return function(dispatch) {
    axios
      .get(api_base_url + '/voyageLocations' + '?id=' + id.toString())
      .then(response => {
        console.log('fetched.', response.data);
        if (Array.isArray(response.data)) {
          dispatch({
            type: 'VOYAGE_LOCATIONS_RECEIVED',
            payload: response.data
          });
        } else if (!Array.isArray(response.data)) {
          dispatch({
            type: 'VOYAGE_LOCATIONS_FAILED',
            payload: response.data
          });
        }
      })
      .catch(err => {
        console.log('fetched.', err);
        dispatch({
          type: 'VOYAGE_LOCATIONS_FAILED',
          payload: err
        });
      });
  };
}
