import axios from 'axios';
import { api_base_url } from '../config'

export function authenticateUser(userName, userPassword,token){
    return function(dispatch){
        if(!token){
        axios.post(api_base_url + "/authenticate", {username: userName, password: userPassword})
        .then((response)=>{
            console.log("Logging in", response.data);
            if(response.data.status){
                dispatch({
                    type: "AUTHENTICATION_SUCCESSFUL", payload: response.data
                })
            }else{
                dispatch({
                    type: "AUTHENTICATION_FAILED", payload: response.data
                })
            }
        })
        .catch((err)=>{
            dispatch({
                type: "AUTHENTICATION_DENIED", payload: err
            })
        })
    }
    else {
        axios.post(api_base_url+ "/authenticate", {token: token})
        .then((response)=>{
            console.log("Logging tried by token", response.data.token)
            if(response.data.status){
                dispatch({
                    type: "AUTHENTICATION_SUCCESSFUL", payload: response.data
                })
            }else{
                dispatch({
                    type: "AUTHENTICATION_FAILED", payload: response.data
                })
            }
        })
        .catch((err)=>{
            dispatch({
                type: "AUTHENTICATION_DENIED", payload: err
            })
        }) 
    }}
}


export function updatePassword(mID, oldPassword, newPassword,token){
    return function(dispatch){
        axios.put(api_base_url + "/passwordupdate", {mID:mID, oldPassword:oldPassword, newPassword:newPassword,token:token})
        .then((response)=>{
            console.log("updating", response.data);
            if(response.data.status){
                dispatch({
                    type: "NEW_PASSWORD_UPDATED_SUCCESSFULLY", payload: response.data
                })
            }
            else{
                dispatch({
                    type: "NEW_PASSWORD_UPDATED_PARAMETER_WRONG", payload: response.data
                })
            }
        })
        .catch((err)=>{
            dispatch({
                type: "NEW_PASSWORD_UPDATED_ERROR", payload: err
            })
        })
    }
    
}