export default function reducer(
  state = {
    liveHealthTable: [
      {
        vesselname: 'SINAR SUMBA',
        vesselcode: 'TPH0019',
        flag: 0,
        servicename: 'TPH',
        speed: { value: 13.8, status: 'positive' },
        'route-deviation': { value: 11, status: 'positive' },
        weather: { value: 1, status: 'positive' },
        'next-port': { value: '2018-10-05 13:00', status: 'positive' },
        bunker: { value: 48, status: 'positive' },
        deviation: { value: 1.4, status: 'positive' },
        distance: { value: 963, status: 'positive' },
        lastupdate: { value: '2018-10-05 11:19', status: 'normal1' }
      },
      {
        vesselname: 'SINAR SABANG',
        vesselcode: 'SLC0036',
        flag: 0,
        servicename: 'SLC',
        speed: { value: 13.1, status: 'positive' },
        'route-deviation': { value: 5, status: 'positive' },
        weather: { value: 5, status: 'normal' },
        'next-port': { value: '2018-10-04 05:00', status: 'positive' },
        bunker: { value: 94, status: 'positive' },
        deviation: { value: -2, status: 'positive' },
        distance: { value: 1242, status: 'positive' },
        lastupdate: { value: '2018-10-05 09:33', status: 'normal1' }
      },
      {
        vesselname: 'SINAR BIMA',
        vesselcode: 'SVC0056',
        flag: 1,
        servicename: 'SVC',
        speed: { value: 14.3, status: 'normal' },
        'route-deviation': { value: 32, status: 'normal' },
        weather: { value: 1, status: 'positive' },
        'next-port': { value: '2018-10-07 11:00', status: 'normal' },
        bunker: { value: 63, status: 'normal' },
        deviation: { value: -6, status: 'normal' },
        distance: { value: 2698, status: 'normal' },
        lastupdate: { value: '2018-10-05 06:54', status: 'normal1' }
      },
      {
        vesselname: 'NORDLION',
        vesselcode: 'GSKC0011',
        flag: 2,
        servicename: 'GSKC',
        speed: { value: 8.4, status: 'negative' },
        'route-deviation': { value: 64, status: 'negative' },
        weather: { value: 8, status: 'negative' },
        'next-port': { value: '2018-10-06 23:00', status: 'negative' },
        bunker: { value: 87, status: 'negative' },
        deviation: { value: 12, status: 'negative' },
        distance: { value: 1861, status: 'negative' },
        lastupdate: { value: '2018-10-05 14:46', status: 'normal1' }
      }
    ]
  },
  action
) {
  switch (action.type) {
    default: {
      return { ...state };
    }
  }
}
