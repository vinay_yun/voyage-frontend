export default function reducer(
  state = {
    tab: 'map'
  },
  action
) {
  switch (action.type) {
    case 'TAB_CHANGED': {
      return { ...state, tab: action.payload };
    }
    case 'TAB_RESET': {
      return { ...state, tab: 'map' };
    }
    default: {
      return { ...state };
    }
  }
}
