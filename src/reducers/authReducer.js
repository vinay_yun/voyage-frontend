export default function reducer(
  state = {
    loginSuccess: true,
    token: '',
    managerId: 0,
    userName: 'Vincent',
    userPassword: '',
    userLoggedOut: false,
    firstLogin: '',
    openPasswordChangeModal: false,
    oldPasswordText: '',
    newPasswordText: '',
    retypeNewPasswordText: '',
    newPasswordUpdateMessage: '',
    newPasswordUpdated: false,
    loginMessage: ''
  },
  action
) {
  switch (action.type) {
    case 'AUTHENTICATION_SUCCESSFUL': {
      // console.log("AUTHENTICATION_SUCCESSFUL", action.payload);
      return {
        ...state,
        token: action.payload.token,
        loginMessage: '',
        loginSuccess: true,
        userLoggedOut: false,
        userPassword: '',
        managerId: action.payload.mId,
        firstLogin: action.payload.firstLogin,
        openPasswordChangeModal: !action.payload.firstLogin
      };
    }
    case 'AUTHENTICATION_FAILED': {
      // console.log("AUTHENTICATION_FAILED", action.payload);
      return {
        ...state,
        userPassword: '',
        userName: '',
        loginSuccess: false,
        loginMessage: action.payload.message
      };
    }
    case 'USER_LOGGED_OUT': {
      return {
        ...state,
        loginSuccess: false,
        token: '',
        managerId: 7,
        userName: '',
        userPassword: '',
        userLoggedOut: true
      };
    }
    case 'USER_NAME_CHANGED': {
      // console.log("USER_NAME_CHANGED", action.payload);
      return { ...state, userName: action.payload };
    }
    case 'USER_PASSWORD_CHANGED': {
      // console.log("USER_PASSWORD_CHANGED", action.payload);
      return { ...state, userPassword: action.payload };
    }
    case 'PASSWORD_CHANGE_MODAL_CLOSED': {
      return { ...state, openPasswordChangeModal: action.payload };
    }

    case 'NEW_PASSWORD_CHANGE': {
      return { ...state, openPasswordChangeModal: action.payload };
    }
    case 'OLD_PASSWORD_TEXT_CHANGE': {
      return { ...state, oldPasswordText: action.payload };
    }

    case 'NEW_PASSWORD_TEXT_CHANGE': {
      return { ...state, newPasswordText: action.payload };
    }
    case 'RETYPE_NEW_PASSWORD_TEXT_CHANGE': {
      return { ...state, retypeNewPasswordText: action.payload };
    }
    case 'NEW_PASSWORD_UPDATED_SUCCESSFULLY': {
      return {
        ...state,
        newPasswordUpdateMessage: action.payload.message,
        newPasswordUpdated: true
      };
    }
    case 'NEW_PASSWORD_UPDATED_PARAMETER_WRONG': {
      return {
        ...state,
        newPasswordUpdateMessage: action.payload.message,
        newPasswordUpdated: false
      };
    }
    case 'NEW_PASSWORD_UPDATED_ERROR': {
      return {
        ...state,
        newPasswordUpdateMessage: 'error in updating password',
        newPasswordUpdated: false
      };
    }
    default: {
      return { ...state };
    }
  }
}
