import { combineReducers } from 'redux';
import mapSearchData from './mapSearchReducer';
import authData from './authReducer';
import tabData from './tabReducer';
import voyagesData from './voyagesReducer';
import liveHealthData from './liveHealthReducer';
import servicesData from './serviceDetailsReducer';
import analyticsData from './analyticsReducer';

const appReducer = combineReducers({
  authData: authData,
  tabData: tabData,
  mapSearchData: mapSearchData,
  voyagesData: voyagesData,
  liveHealthData: liveHealthData,
  servicesData: servicesData,
  analyticsData: analyticsData
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGGED_OUT') {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
