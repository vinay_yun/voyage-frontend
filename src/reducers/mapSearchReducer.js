export default function reducer(
  state = {
    searchlist: [
      {
        carRegiNum: 'SINAR SABANG',
        deviceChecked: true,
        dName: 'SINGAPORE-LAEM CHABANG',
        dDeviceID: 'SLC0036'
      },
      {
        carRegiNum: 'SINAR BIMA',
        deviceChecked: true,
        dName: 'BANGKOK-BUSAN',
        dDeviceID: 'SVC0056'
      },
      {
        carRegiNum: 'SINAR SUMBA',
        deviceChecked: true,
        dName: 'INCHEON-BELAWAN',
        dDeviceID: 'TPH0019'
      },
      {
        carRegiNum: 'NORDLION',
        deviceChecked: true,
        dName: 'SINGAPORE-MUMBAI',
        dDeviceID: 'GSKC0011'
      }
    ],
    searchtext: '',
    checkboxstate: true,
    deviceIDSelected: 'SLC0036',
    deviceLocations: [
      {
        lat: 0.654673,
        lng: 105.407098,
        showMarker: true,
        dDeviceID: 'SLC0036'
      },
      {
        lat: 3.359475,
        lng: 100.292672,
        showMarker: true,
        dDeviceID: 'SVC0056'
      },
      {
        lat: 20.807547,
        lng: 115.678856,
        showMarker: true,
        dDeviceID: 'TPH0019'
      },
      { lat: 7.531437, lng: 78.070114, showMarker: true, dDeviceID: 'GSKC0011' }
    ],
    reloadSearchList: 0,
    polylineDict: {},
    center: { lat: 0.654673, lng: 105.407098 },
    infoContent: '',
    distance: '',
    eta: '',
    outLocation: '',
    polyline: {},
    animatePolyline: {},
    centerChanged: false,
    mainMapCenter: { lat: 0.654673, lng: 105.407098 },
    voyageDetails: [
      {
        serviceNumber: 'SLC0036',
        legID: 0,
        portCall: 'SIN',
        arrivalTime: '2018-10-19 04:47',
        estiArrivalTime: '2018-10-18 20:00',
        departureTime: '2018-10-19 22:19',
        EstiDepartureTime: '2018-10-19 20:00',
        netSpeed: null,
        estiNetSpeed: null,
        distanceTravelled: null,
        estiDistaneTravelled: null,
        portTime: 24,
        estiPortTime: 25,
        seaTime: null,
        estiSeaTime: null,
        mvsPerHour: 69,
        estiMvsPerHOur: 70,
        totalMoves: 1400,
        estiTotalMoves: 1400,
        bunkerUsage: null,
        estiBunkerUsage: null,
        idleTime: 0
      },
      {
        serviceNumber: 'SLC0036',
        legID: 1,
        portCall: 'LCB',
        arrivalTime: '2018-10-21 07:21',
        estiArrivalTime: '2018-10-21 05:00',
        departureTime: '2018-10-22 08:54',
        EstiDepartureTime: '2018-10-22 05:00',
        netSpeed: 12.3,
        estiNetSpeed: 13.5,
        distanceTravelled: 861,
        estiDistaneTravelled: 795,
        portTime: 23,
        estiPortTime: 25,
        seaTime: 64,
        estiSeaTime: 60,
        mvsPerHour: 67,
        estiMvsPerHOur: 70,
        totalMoves: 1400,
        estiTotalMoves: 1400,
        bunkerUsage: 58,
        estiBunkerUsage: 52,
        idleTime: 0
      },
      {
        serviceNumber: 'SLC0036',
        legID: 2,
        portCall: 'SIN',
        arrivalTime: 'Ongoing',
        estiArrivalTime: '2018-10-25 20:00',
        departureTime: 'NA',
        EstiDepartureTime: '2018-10-26 20:00',
        netSpeed: 13.2,
        estiNetSpeed: 13.5,
        distanceTravelled: 412,
        estiDistaneTravelled: 795,
        portTime: 'NA',
        estiPortTime: 25,
        seaTime: 34,
        estiSeaTime: 60,
        mvsPerHour: 'NA',
        estiMvsPerHOur: 70,
        totalMoves: 'NA',
        estiTotalMoves: 1400,
        bunkerUsage: 41,
        estiBunkerUsage: 52,
        idleTime: 2
      }
    ],
    voyageDetailsNew: {
      SLC0036: [
        {
          serviceNumber: 'SLC0036',
          legID: 0,
          portCall: 'SIN',
          arrivalTime: '2018-07-19 04:47',
          estiArrivalTime: '2018-07-18 20:00',
          departureTime: '2018-07-19 22:19',
          EstiDepartureTime: '2018-07-19 20:00',
          netSpeed: null,
          estiNetSpeed: null,
          distanceTravelled: null,
          estiDistaneTravelled: null,
          portTime: 24,
          estiPortTime: 25,
          seaTime: null,
          estiSeaTime: null,
          mvsPerHour: 69,
          estiMvsPerHOur: 70,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: null,
          estiBunkerUsage: null,
          idleTime: 0
        },
        {
          serviceNumber: 'SLC0036',
          legID: 1,
          portCall: 'LCB',
          arrivalTime: '2018-07-21 07:21',
          estiArrivalTime: '2018-07-21 05:00',
          departureTime: '2018-07-22 08:54',
          EstiDepartureTime: '2018-07-22 05:00',
          netSpeed: 12.3,
          estiNetSpeed: 13.5,
          distanceTravelled: 861,
          estiDistaneTravelled: 795,
          portTime: 23,
          estiPortTime: 25,
          seaTime: 64,
          estiSeaTime: 60,
          mvsPerHour: 67,
          estiMvsPerHOur: 70,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: 58,
          estiBunkerUsage: 52,
          idleTime: 0
        },
        {
          serviceNumber: 'SLC0036',
          legID: 2,
          portCall: 'SIN',
          arrivalTime: 'Ongoing',
          estiArrivalTime: '2018-07-25 20:00',
          departureTime: 'NA',
          EstiDepartureTime: '2018-07-26 20:00',
          netSpeed: 13.2,
          estiNetSpeed: 13.5,
          distanceTravelled: 412,
          estiDistaneTravelled: 795,
          portTime: 'NA',
          estiPortTime: 25,
          seaTime: 34,
          estiSeaTime: 60,
          mvsPerHour: 'NA',
          estiMvsPerHOur: 70,
          totalMoves: 'NA',
          estiTotalMoves: 1400,
          bunkerUsage: 41,
          estiBunkerUsage: 52,
          idleTime: 2
        }
      ],
      GSKC0011: [
        {
          serviceNumber: 'GSKC0011',
          legID: 0,
          portCall: 'SIN',
          arrivalTime: '2018-07-19 04:47',
          estiArrivalTime: '2018-07-18 20:00',
          departureTime: '2018-07-19 22:19',
          EstiDepartureTime: '2018-07-19 20:00',
          netSpeed: null,
          estiNetSpeed: null,
          distanceTravelled: null,
          estiDistaneTravelled: null,
          portTime: 24,
          estiPortTime: 25,
          seaTime: null,
          estiSeaTime: null,
          mvsPerHour: 69,
          estiMvsPerHOur: 70,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: null,
          estiBunkerUsage: null,
          idleTime: 0
        },
        {
          serviceNumber: 'SLC0036',
          legID: 1,
          portCall: 'JNPT',
          arrivalTime: '2018-10-21 07:21',
          estiArrivalTime: '2018-10-21 05:00',
          departureTime: '2018-10-22 08:54',
          EstiDepartureTime: '2018-10-22 05:00',
          netSpeed: 12.3,
          estiNetSpeed: 13.5,
          distanceTravelled: 861,
          estiDistaneTravelled: 795,
          portTime: 23,
          estiPortTime: 25,
          seaTime: 64,
          estiSeaTime: 60,
          mvsPerHour: 67,
          estiMvsPerHOur: 70,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: 58,
          estiBunkerUsage: 52,
          idleTime: 0
        },
        {
          serviceNumber: 'SLC0036',
          legID: 2,
          portCall: 'SIN',
          arrivalTime: 'Ongoing',
          estiArrivalTime: '2018-10-25 20:00',
          departureTime: 'NA',
          EstiDepartureTime: '2018-10-26 20:00',
          netSpeed: 13.2,
          estiNetSpeed: 13.5,
          distanceTravelled: 412,
          estiDistaneTravelled: 795,
          portTime: 'NA',
          estiPortTime: 25,
          seaTime: 34,
          estiSeaTime: 60,
          mvsPerHour: 'NA',
          estiMvsPerHOur: 70,
          totalMoves: 'NA',
          estiTotalMoves: 1400,
          bunkerUsage: 41,
          estiBunkerUsage: 52,
          idleTime: 2
        }
      ],
      TPH0019: [
        {
          serviceNumber: 'TPH0019',
          legID: 0,
          portCall: 'KAO',
          arrivalTime: '2018-07-03 19:47',
          estiArrivalTime: '2018-07-03 17:00',
          departureTime: '2018-07-04 14:21',
          EstiDepartureTime: '2018-07-04 14:00',
          netSpeed: null,
          estiNetSpeed: null,
          distanceTravelled: null,
          estiDistaneTravelled: null,
          portTime: 22,
          estiPortTime: 21,
          seaTime: null,
          estiSeaTime: null,
          mvsPerHour: 77,
          estiMvsPerHOur: 80,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: null,
          estiBunkerUsage: null,
          idleTime: 0
        },
        {
          serviceNumber: 'TPH0019',
          legID: 1,
          portCall: 'CDO',
          arrivalTime: '2018-07-08 07:45',
          estiArrivalTime: '2018-07-08 08:00',
          departureTime: '2018-07-08 18:51',
          EstiDepartureTime: '2018-07-08 19:00',
          netSpeed: 13,
          estiNetSpeed: 13.2,
          distanceTravelled: 1224,
          estiDistaneTravelled: 1200,
          portTime: 18,
          estiPortTime: 18,
          seaTime: 92,
          estiSeaTime: 91,
          mvsPerHour: 28,
          estiMvsPerHOur: 30,
          totalMoves: 300,
          estiTotalMoves: 300,
          bunkerUsage: 102,
          estiBunkerUsage: 96,
          idleTime: 1
        },
        {
          serviceNumber: 'TPH0019',
          legID: 2,
          portCall: 'GEN',
          arrivalTime: '2018-07-10 14:22',
          estiArrivalTime: '2018-07-10 13:00',
          departureTime: '2018-07-10 04:16',
          EstiDepartureTime: '2018-07-11 05:00',
          netSpeed: 12,
          estiNetSpeed: 12,
          distanceTravelled: 458,
          estiDistaneTravelled: 450,
          portTime: 21,
          estiPortTime: 20.5,
          seaTime: 39,
          estiSeaTime: 38,
          mvsPerHour: 39,
          estiMvsPerHOur: 40,
          totalMoves: 500,
          estiTotalMoves: 500,
          bunkerUsage: 39.4,
          estiBunkerUsage: 42,
          idleTime: 2
        },
        {
          serviceNumber: 'TPH0019',
          legID: 3,
          portCall: 'DAV',
          arrivalTime: '2018-07-12 09:32',
          estiArrivalTime: '2018-07-12 08:00',
          departureTime: '2018-07-14 02:43',
          EstiDepartureTime: '2018-07-14 01:00',
          netSpeed: 10.6,
          estiNetSpeed: 10,
          distanceTravelled: 102,
          estiDistaneTravelled: 100,
          portTime: 41,
          estiPortTime: 39,
          seaTime: 9.5,
          estiSeaTime: 9.6,
          mvsPerHour: 30,
          estiMvsPerHOur: 30,
          totalMoves: 600,
          estiTotalMoves: 600,
          bunkerUsage: 8.1,
          estiBunkerUsage: 8,
          idleTime: 1
        },
        {
          serviceNumber: 'TPH0019',
          legID: 4,
          portCall: 'KAO',
          arrivalTime: 'Ongoing',
          estiArrivalTime: '2018-07-17 17:00',
          departureTime: 'NA',
          EstiDepartureTime: '2018-07-18 14:00',
          netSpeed: 15,
          estiNetSpeed: 14.8,
          distanceTravelled: 954,
          estiDistaneTravelled: 1350,
          portTime: null,
          estiPortTime: 23,
          seaTime: 59,
          estiSeaTime: 92,
          mvsPerHour: null,
          estiMvsPerHOur: 80,
          totalMoves: 1400,
          estiTotalMoves: 1400,
          bunkerUsage: 82,
          estiBunkerUsage: 108,
          idleTime: 3
        }
      ],
      SVC0056: [
        {
          serviceNumber: 'SVC0056',
          legID: 0,
          portCall: 'SIN',
          arrivalTime: '2018-07-19 04:47',
          estiArrivalTime: '2018-07-18 20:00',
          departureTime: '2018-07-19 22:19',
          EstiDepartureTime: '2018-07-19 20:00',
          netSpeed: null,
          estiNetSpeed: null,
          distanceTravelled: null,
          estiDistaneTravelled: null,
          portTime: 19.75,
          estiPortTime: 19.29,
          seaTime: null,
          estiSeaTime: null,
          mvsPerHour: 70,
          estiMvsPerHOur: 70,
          totalMoves: 100,
          estiTotalMoves: 100,
          bunkerUsage: null,
          estiBunkerUsage: null,
          idleTime: 0
        },
        {
          serviceNumber: 'SVC0056',
          legID: 1,
          portCall: 'BLW',
          arrivalTime: '2018-07-21 11:21',
          estiArrivalTime: '2018-07-21 12:00',
          departureTime: '2018-07-22 14:33',
          EstiDepartureTime: '2018-07-22 15:00',
          netSpeed: 13.2,
          estiNetSpeed: 13.2,
          distanceTravelled: 374,
          estiDistaneTravelled: 354,
          portTime: 37.2,
          estiPortTime: 38,
          seaTime: 28,
          estiSeaTime: 26.4,
          mvsPerHour: 67,
          estiMvsPerHOur: 70,
          totalMoves: 1000,
          estiTotalMoves: 1000,
          bunkerUsage: 33.2,
          estiBunkerUsage: 30.8,
          idleTime: 0
        },
        {
          serviceNumber: 'SVC0056',
          legID: 2,
          portCall: 'SIN',
          arrivalTime: 'Ongoing',
          estiArrivalTime: '2018-07-25 20:00',
          departureTime: 'NA',
          EstiDepartureTime: '2018-07-26 20:00',
          netSpeed: 13.8,
          estiNetSpeed: 13.2,
          distanceTravelled: 234,
          estiDistaneTravelled: 354,
          portTime: 'NA',
          estiPortTime: 25,
          seaTime: 18,
          estiSeaTime: 26.4,
          mvsPerHour: 'NA',
          estiMvsPerHOur: 70,
          totalMoves: 'NA',
          estiTotalMoves: 1000,
          bunkerUsage: 26,
          estiBunkerUsage: 30.8,
          idleTime: 2
        }
      ]
    }
  },
  action
) {
  switch (action.type) {
    case 'RECEIVED_LOCATIONS': {
      return { ...state };
    }

    case 'SEARCH_TEXT_CHANGED': {
      return { ...state, searchtext: action.payload };
    }

    case 'CHECKBOX_STATE_CHANGED': {
      let locArray = [];
      for (let i = 0; i < state.deviceLocations.length; i++) {
        const loc = state.deviceLocations[i];
        loc.showMarker = action.payload;
        //console.log("hello show marker", action.payload);
        locArray.push(loc);
      }
      let deviceRegnumArray = [];
      for (let i = 0; i < state.searchlist.length; i++) {
        const deviceRegnum = state.searchlist[i];

        deviceRegnum.deviceChecked = action.payload;
        deviceRegnumArray.push(deviceRegnum);
      }

      return {
        ...state,
        deviceLocations: locArray,
        checkboxstate: action.payload,
        searchlist: deviceRegnumArray
      };
    }

    case 'LIST_CHECKBOX_STATUS_CHANGED': {
      let locArray = [];
      let tempState = true;
      let tempCenter = {};
      let tempMainCenterChangeArray = [];
      for (let i = 0; i < state.deviceLocations.length; i++) {
        // console.log("what",state.deviceLocations[i].deviceID, typeof(state.deviceLocations[i].deviceID), action.payload, typeof(action.payload))
        if (state.deviceLocations[i].dDeviceID === action.payload.toString()) {
          const loc = state.deviceLocations[i];
          loc.showMarker = !loc.showMarker;
          if (loc.showMarker) {
            tempCenter = { lat: loc.lat, lng: loc.lng };
            // console.log(" tempCenter", tempCenter)
          }
          locArray.push(loc);
        } else {
          locArray.push(state.deviceLocations[i]);
        }
        tempState = tempState && locArray[i].showMarker;
      }

      // code to change map center of cars on map
      if (locArray.length > 0) {
        for (let j = 0; j < locArray.length; j++) {
          if (locArray[j].showMarker) {
            //   if(locArray[j].deviceID === action.payload.toString())
            //   {
            //     tempCenter = { lat:parseFloat( locArray[j].lat) , lng:parseFloat( locArray[j].lng)  }
            //   }
            const clickedCheckBoxItem = locArray[j];
            tempMainCenterChangeArray.push(clickedCheckBoxItem);
          }
        }
      }

      if (tempMainCenterChangeArray.length > 0) {
        // last element location as mapcenter
        // tempCenter = { lat: parseFloat(tempMainCenterChangeArray[tempMainCenterChangeArray.length - 1].lat), lng: parseFloat(tempMainCenterChangeArray[tempMainCenterChangeArray.length - 1].lng) }

        for (let j = 0; j < tempMainCenterChangeArray.length; j++) {
          if (
            tempMainCenterChangeArray[j].dDeviceID === action.payload.toString()
          ) {
            tempCenter = {
              lat: parseFloat(tempMainCenterChangeArray[j].lat),
              lng: parseFloat(tempMainCenterChangeArray[j].lng)
            };
          }
        }
        // console.log(" tempCenter",  tempCenter)
      }

      //
      let deviceRegnumArray = [];
      for (let i = 0; i < state.searchlist.length; i++) {
        console.log(
          state.searchlist[i].dDeviceID,
          typeof state.searchlist[i].dDeviceID,
          action.payload,
          typeof action.payload
        );
        if (state.searchlist[i].dDeviceID === action.payload) {
          console.log(
            'equal: ',
            state.searchlist[i].dDeviceID,
            typeof state.searchlist[i].dDeviceID,
            action.payload,
            typeof action.payload
          );
          let deviceRegnum = state.searchlist[i];
          deviceRegnum.deviceChecked = !deviceRegnum.deviceChecked;
          deviceRegnumArray.push(deviceRegnum);
        } else {
          deviceRegnumArray.push(state.searchlist[i]);
        }
      }
      return {
        ...state,
        deviceLocations: locArray,
        mainMapCenter: tempCenter,
        checkboxstate: tempState,
        searchlist: deviceRegnumArray,
        deviceIDSelected: action.payload
      };
    }

    default: {
      return { ...state };
    }
  }
}
