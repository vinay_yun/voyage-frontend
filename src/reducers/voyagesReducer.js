export default function reducer(
  state = {
    allVoyages: [],
    voyageLocations: [],
    startLoc: {},
    endLoc: {}
  },
  action
) {
  switch (action.type) {
    case 'ALL_VOYAGES_RECEIVED': {
      return { ...state, allVoyages: action.payload };
    }
    case 'VOYAGE_LOCATIONS_RECEIVED': {
      let tempstartLoc = {};
      let tempendLoc = {};
      if (action.payload.length > 0) {
        tempstartLoc = action.payload[0];
        tempendLoc = action.payload[action.payload.length - 1];
      }

      return {
        ...state,
        voyageLocations: action.payload,
        startLoc: tempstartLoc,
        endLoc: tempendLoc
      };
    }
    default: {
      return { ...state };
    }
  }
}
