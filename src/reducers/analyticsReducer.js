export default function reducer(
  state = {
    serviceSelected: '',
    bunkerConsumption: [
      {
        name: 'Fleet',
        totalConsumed: 48340,
        expectedConsumption: 44800,
        bunkerConsumedKgPerNM: 0.086 * 1000,
        bunkerConsumedKgPerContainer: 0.095 * 1000
      },
      {
        name: 'TPH',
        totalConsumed: 18140,
        expectedConsumption: 16420,
        bunkerConsumedKgPerNM: 0.089 * 1000,
        bunkerConsumedKgPerContainer: 0.145 * 1000
      },
      {
        name: 'SLC',
        totalConsumed: 16144,
        expectedConsumption: 15220,
        bunkerConsumedKgPerNM: 0.079 * 1000,
        bunkerConsumedKgPerContainer: 0.063 * 1000
      },
      {
        name: 'SVC',
        totalConsumed: 14056,
        expectedConsumption: 13160,
        bunkerConsumedKgPerNM: 0.084 * 1000,
        bunkerConsumedKgPerContainer: 0.087 * 1000
      }
    ],
    reliabilityReport: {
      timeDelays: [
        { name: '0 to 2 hrs', vessels: 34 },
        { name: '2 to 6 hrs', vessels: 18 },
        { name: '6 to 12 hrs', vessels: 13 },
        { name: 'more than 12 hrs', vessels: 7 },
        { name: '-2 to 0 hrs', vessels: 11 },
        { name: '-6 to -2 hrs', vessels: 5 },
        { name: '-12 to -6 hrs', vessels: 2 },
        { name: 'less than -12 hrs', vessels: 1 }
      ],
      timeDelaysPie: [
        { name: '0 to 2 hrs', value: 34 },
        { name: '2 to 6 hrs', value: 18 },
        { name: '6 to 12 hrs', value: 13 },
        { name: 'more than 12 hrs', value: 7 },
        { name: '-2 to 0 hrs', value: 11 },
        { name: '-6 to -2 hrs', value: 5 },
        { name: '-12 to -6 hrs', value: 2 },
        { name: 'less than -12 hrs', value: 1 }
      ],
      serviceDelays: [
        { name: 'Fleet', hours: 6.6 },
        { name: 'TPH', hours: 8.2 },
        { name: 'SLC', hours: 4.7 },
        { name: 'SVC', hours: 5.3 }
      ],
      serviceEarly: [
        { name: 'Fleet', hours: 2.9 },
        { name: 'TPH', hours: 1.4 },
        { name: 'SLC', hours: 4.1 },
        { name: 'SVC', hours: 2.3 }
      ]
    },
    weatherReport: {
      impacts: [
        { name: 'Impacted', value: 13 },
        { name: 'Not Impacts', value: 76 }
      ],
      percentImpacts: [
        { name: 'Impacted %', value: 17 },
        { name: 'Not Impacted %', value: 83 }
      ]
    },
    weatherReportWithLevels: [
      { name: 1, value: 424 },
      { name: 2, value: 357 },
      { name: 3, value: 254 },
      { name: 4, value: 139 },
      { name: 5, value: 64 },
      { name: 6, value: 21 },
      { name: 7, value: 162 },
      { name: 8, value: 46 },
      { name: 9, value: 22 },
      { name: 10, value: 34 }
    ],
    terminalReport: {}
  },
  action
) {
  switch (action.type) {
    case 'ANALYTICS_SERVICE_CHANGED': {
      return { ...state, serviceSelected: action.payload };
    }
    default: {
      return { ...state };
    }
  }
}
